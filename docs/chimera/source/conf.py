# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import tomlkit

# -- Helpers --------------------------------------------------------------

# https://github.com/wemake-services/wemake-python-styleguide/blob/master/docs/conf.py#L22


def project_name_from_toml():
    with open("../../../pyproject.toml") as pyproject:
        file_contents = pyproject.read()

    return str(tomlkit.parse(file_contents)["tool"]["poetry"]["name"])


def project_version_from_toml():
    with open("../../../pyproject.toml") as pyproject:
        file_contents = pyproject.read()

    return str(tomlkit.parse(file_contents)["tool"]["poetry"]["version"])


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = project_name_from_toml()
copyright = "2023, Cavelle Benjamin"
author = "Cavelle Benjamin"

# The short X.Y version
version = project_version_from_toml()

# The full version, including alpha/beta/rc tags
release = project_version_from_toml()

rst_epilog = """
.. |ProjectVersion| replace:: {project} ({versionnum})
""".format(
    project=project,
    versionnum=version,
)

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx_click",
]

templates_path = ["_templates"]
exclude_patterns = []

language = "en"

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "alabaster"
html_static_path = ["_static"]
