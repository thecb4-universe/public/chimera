# Chimera

Create all kinds of projects with one tool

[![pipeline status](https://gitlab.com/thecb4-universe/public/chimera/badges/main/pipeline.svg)](https://gitlab.com/thecb4-universe/public/chimera/-/commits/main)

[![coverage report](https://gitlab.com/thecb4-universe/public/chimera/badges/main/coverage.svg)](https://gitlab.com/thecb4-universe/public/chimera/-/commits/main)

[![Latest Release](https://gitlab.com/thecb4-universe/public/chimera/-/badges/release.svg)](https://gitlab.com/thecb4-universe/public/chimera/-/releases)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built With

* [poetry](https://python-poetry.org)
* [click](https://palletsprojects.com/p/click/)
* [snakemd](https://www.snakemd.io/en/latest/)

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

Package Manager

```shell
code steps for package manager
```

## Using

### Create a project 

```shell
chimera create pegasus
```

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Roadmap and Contributing

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

### Roadmap

Please read [ROADMAP](ROADMAP.md) for an outline of how we would like to evolve the library.

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for changes to us.

### Changes

Please read [CHANGELOG](CHANGELOG.md) for details on changes to the library. 

## Authors

* **$(AUTHOR)** - *Initial work* - [$(AUTHOR_HANDLE)]($(AUTHOR_WEBSITE))

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

## References

* (Artifacts as release assets](https://stackoverflow.com/questions/67570911/gitlab-how-to-include-the-previous-jobs-artifacts-as-release-assets)
* [Virtual Env in project](https://stackoverflow.com/questions/71239764/how-to-cache-poetry-install-for-gitlab-ci)
