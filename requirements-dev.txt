pytest==7.2.2
unittest2==1.1.0
pytest-cov==4.0.0
coverage[toml]==7.2.2
black==23.3.0
ruff==0.0.260
click==8.1.3
sphinx-click==4.4.0
tomlkit==0.11.7