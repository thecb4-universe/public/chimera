# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.9] - YYYY-MMM-dd

### Added

- documentation release to github pages through CI

### Changed

### Deprecated

### Removed

### Fixed
- fixed cache directories

## Security

## [0.1.8] - 2023-Apr-07

### Added

- added job rules for all other jobs

### Changed

- rule for publish only runs on tag creation

### Deprecated

### Removed

### Fixed

## Security

## [0.1.7] - 2023-Apr-07

### Added

- CI access token for project
- publish job to gitlab-ci file
- rule in gitlab ci to only run jobs on main commit (don't rerun when tag is created)
- git actions to make file for add, commit, push

### Changed

### Deprecated

### Removed

### Fixed

## Security

## [0.1.6] - 2023-Apr-07

### Added

- use of unittest to add teardown inside of test case (moving it from makefile)
- subdirectories can have base configurations
- test artifacts to gitlab ci

### Changed

- markdowns is now labeled as markdown
- files is now labeled as texts

### Deprecated

### Removed

### Fixed

## Security

## [0.1.5] - 2023-Apr-05

### Added

- create directories from yaml

### Changed

### Deprecated

### Removed

### Fixed

## Security

## [0.1.4] - 2023-Apr-04

### Added

- project yaml files can now use a base project structure to start with
- code coverage report as artifact in gitlab

### Changed

### Deprecated

### Removed

### Fixed

## Security

## [0.1.3] - 2023-Apr-04

### Added

### Changed

- refactored yaml reading for each schema
- moved from urllib to requests for getting yaml content
- create profile from url or local file

### Deprecated

### Removed

### Fixed

## Security

## [0.1.2] - 2023-Apr-04

### Added

### Changed

- create project from yaml file

### Deprecated

### Removed

### Fixed

## Security

## [0.1.1] - 2023-Apr-01

### Added

- global cache to reduce minimize pipeline cycletime

### Changed

- image for pipeline customized with make and poetry pre-installed

### Deprecated

### Removed

- make install command removed

### Fixed

## Security

## [0.1.0] - 2023-Apr-01

### Added

- create a project with empty README, LICENSE, CHANGELOG, CONTRIBUTING, and .gitignore files

### Changed

### Deprecated

### Removed

### Fixed

## Security

## [template] - YYYY-MMM-dd

### Added

### Changed

### Deprecated

### Removed

### Fixed

## Security