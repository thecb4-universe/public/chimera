import os
from pathlib import Path
import shutil
import logging
import unittest

from click.testing import CliRunner
from chimera.main import main


class TestProjectCreation(unittest.TestCase):
    temp_folders = []

    def teardown_class(cls):
        log = logging.getLogger(
            "TestChimeraCreate.test_project_is_created_from_local_yaml_with_no_base"
        )

        for folder in TestProjectCreation.temp_folders:
            if os.path.exists(folder):
                try:
                    shutil.rmtree(folder)
                except OSError as e:
                    log.debug("Error: %s - %s." % (e.filename, e.strerror))

    def test_it_should_create_from_local_yaml_with_no_base(self):
        # log = logging.getLogger(
        #     "TestChimeraCreate.test_project_is_created_from_local_yaml_with_no_base"
        # )

        # Given
        runner = CliRunner()
        source_yml = "project_withnobase.yml"
        default_project_yml = Path(os.getcwd()) / "tests/fixtures" / source_yml
        test_base_directory = Path(os.getcwd()) / "tests"
        expected_project_folder = "pegasus"

        # when
        with runner.isolated_filesystem(temp_dir=test_base_directory) as temp_dir:
            working_directory = Path(test_base_directory) / temp_dir
            work_dir_project_yml = Path(working_directory) / "project.yml"

            TestProjectCreation.temp_folders.append(working_directory)
            shutil.copyfile(default_project_yml, work_dir_project_yml)

            result = runner.invoke(main, ["create"])

            # then
            self.assertTrue(not result.exception)
            self.assertTrue(Path(work_dir_project_yml).exists())
            self.assertTrue(Path(expected_project_folder).is_dir())
            self.assertTrue((Path(expected_project_folder) / "README.md").exists())
            self.assertTrue((Path(expected_project_folder) / "LICENSE.md").exists())
            self.assertTrue((Path(expected_project_folder) / "CHANGELOG.md").exists())
            self.assertTrue(
                (Path(expected_project_folder) / "CONTRIBUTING.md").exists()
            )
            self.assertTrue((Path(expected_project_folder) / "ROADMAP.md").exists())
            self.assertTrue((Path(expected_project_folder) / ".gitignore").exists())
            self.assertTrue((Path(expected_project_folder) / "makefile").exists())
            self.assertTrue((Path(expected_project_folder) / "apple").is_dir())
            self.assertTrue((Path(expected_project_folder) / "android").is_dir())
            self.assertTrue((Path(expected_project_folder) / "web").is_dir())

    def test_it_should_createfrom_remote_yaml(self):
        # Given
        runner = CliRunner()
        source_yml = "https://gitlab.com/thecb4-universe/public/chimera/-/snippets/2524309/raw/main/project.yml"
        test_base_directory = Path(os.getcwd()) / "tests"
        expected_project_folder = "pegasus"

        # when
        with runner.isolated_filesystem(temp_dir=test_base_directory):
            result = runner.invoke(main, ["create", source_yml])

            # then
            self.assertTrue(not result.exception)
            self.assertTrue(Path(expected_project_folder).is_dir())
            self.assertTrue((Path(expected_project_folder) / "README.md").exists())
            self.assertTrue((Path(expected_project_folder) / "LICENSE.md").exists())
            self.assertTrue((Path(expected_project_folder) / "CHANGELOG.md").exists())
            self.assertTrue(
                (Path(expected_project_folder) / "CONTRIBUTING.md").exists()
            )
            self.assertTrue((Path(expected_project_folder) / "ROADMAP.md").exists())
            self.assertTrue((Path(expected_project_folder) / ".gitignore").exists())
            self.assertTrue((Path(expected_project_folder) / "makefile").exists())

    def test_it_should_create_from_local_yaml_with_base(self):
        # Given
        runner = CliRunner()
        source_yml = "project_withbase.yml"
        default_project_yml = Path(os.getcwd()) / "tests/fixtures" / source_yml
        test_base_directory = Path(os.getcwd()) / "tests"
        expected_project_folder = "pegasus"

        # when
        with runner.isolated_filesystem(temp_dir=test_base_directory) as temp_dir:
            working_directory = Path(test_base_directory) / temp_dir
            work_dir_project_yml = Path(working_directory) / "project.yml"

            TestProjectCreation.temp_folders.append(working_directory)
            shutil.copyfile(default_project_yml, work_dir_project_yml)

            result = runner.invoke(main, ["create"])

            # then
            self.assertTrue(not result.exception)
            self.assertTrue(Path(work_dir_project_yml).exists())
            self.assertTrue(Path(expected_project_folder).is_dir())
            self.assertTrue((Path(expected_project_folder) / "README.md").exists())
            self.assertTrue((Path(expected_project_folder) / "LICENSE.md").exists())
            self.assertTrue((Path(expected_project_folder) / "CHANGELOG.md").exists())
            self.assertTrue(
                (Path(expected_project_folder) / "CONTRIBUTING.md").exists()
            )
            self.assertTrue((Path(expected_project_folder) / "ROADMAP.md").exists())
            self.assertTrue(
                (Path(expected_project_folder) / "ACKNOWLEDGEMENTS.md").exists()
            )
            self.assertTrue((Path(expected_project_folder) / ".gitignore").exists())
            self.assertTrue((Path(expected_project_folder) / "makefile").exists())
            self.assertTrue((Path(expected_project_folder) / ".gitignore").exists())

    def test_it_should_create_subdirectory_content(self):
        # Given
        runner = CliRunner()
        source_yml = "project_with_subdirectories.yml"
        default_project_yml = Path(os.getcwd()) / "tests/fixtures" / source_yml
        test_base_directory = Path(os.getcwd()) / "tests"
        expected_project_folder = "pegasus"

        # when
        with runner.isolated_filesystem(temp_dir=test_base_directory) as temp_dir:
            working_directory = Path(test_base_directory) / temp_dir
            work_dir_project_yml = Path(working_directory) / "project.yml"

            TestProjectCreation.temp_folders.append(working_directory)
            shutil.copyfile(default_project_yml, work_dir_project_yml)

            result = runner.invoke(main, ["create"])

            # then
            self.assertTrue(not result.exception)
            self.assertTrue(Path(work_dir_project_yml).exists())
            self.assertTrue(Path(expected_project_folder).is_dir())
            self.assertTrue((Path(expected_project_folder) / "README.md").exists())
            self.assertTrue((Path(expected_project_folder) / "LICENSE.md").exists())
            self.assertTrue((Path(expected_project_folder) / "CHANGELOG.md").exists())
            self.assertTrue(
                (Path(expected_project_folder) / "CONTRIBUTING.md").exists()
            )
            self.assertTrue((Path(expected_project_folder) / "ROADMAP.md").exists())
            self.assertTrue((Path(expected_project_folder) / ".gitignore").exists())
            self.assertTrue((Path(expected_project_folder) / "makefile").exists())
            self.assertTrue((Path(expected_project_folder) / "web").is_dir())
            self.assertTrue((Path(expected_project_folder) / "web/index.html").exists())


if __name__ == "__main__":
    unittest.main()
