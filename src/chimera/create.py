from pathlib import Path
import click
import os
import snakemd
import urllib
import yaml
import requests


@click.command()
@click.argument("name", default="file:./project.yml")
def create(name):
    click.echo("Creating project from " + name)

    source = get_yaml(name)
    create_project_from_yaml(source)


def get_yaml(location):
    schema = get_schema(location)

    match schema:
        case "file":
            return get_yaml_from_local(location)
        case "https":
            return get_yaml_from_remote(location)
        case "http":
            return get_yaml_from_remote(location)
        case _:
            click.echo("Unknown input")
            return ""


def get_yaml_from_local(location):
    click.echo("File input")
    _location = urllib.parse.urlparse(location)
    with open(_location.path, "r", encoding="utf-8") as text:
        source = yaml.safe_load(text)
        return source


def get_yaml_from_remote(location):
    click.echo("HTTP/HTTPS input")
    response = requests.get(location)
    source = response.content
    return yaml.safe_load(source)


def get_schema(uri):
    location = urllib.parse.urlparse(uri)
    return location.scheme


def create_project_from_yaml(project_yml):
    click.echo("Creating project " + project_yml["project"])
    if not os.path.exists(project_yml["project"]):
        os.makedirs(project_yml["project"])
        click.echo("Created directory " + project_yml["project"])

        create_content_for_directory(project_yml, project_yml["project"])


def create_markdown_files(markdown_names, directory):
    for markdown_name in markdown_names:
        markdown = snakemd.new_doc()
        markdown.dump(markdown_name, dir=directory)


def create_text_files(file_names, directory):
    for file_name in file_names:
        open(Path(directory) / file_name, "a").close()


def create_directories(directories, base):
    click.echo(directories)
    for directory in directories:
        if not os.path.exists(directory["name"]):
            next = Path(base) / directory["name"]
            os.makedirs(next)
            click.echo("Created directory " + str(next))
            create_content_for_directory(directory, next)


def create_content_for_directory(content, folder):
    if "base" in content:
        base_yaml = get_yaml(content["base"])

        create_content_for_directory(base_yaml, folder)

    if "markdown" in content:
        markdowns = content["markdown"]
        create_markdown_files(markdowns, folder)

    if "text" in content:
        texts = content["text"]
        create_text_files(texts, folder)

    if "directories" in content:
        directories = content["directories"]
        create_directories(directories, folder)
