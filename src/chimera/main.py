import click

from chimera.create import create


@click.group()
@click.version_option()
def main():
    """Create projects"""
    pass


main.add_command(create)

if __name__ == "__main__":
    main()
