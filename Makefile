
app_name := "chimera"
requirements := $(shell cat requirements.txt)
dev_requirements := $(shell cat requirements-dev.txt)
version := $(shell grep -m 1 version pyproject.toml | tr -s ' ' | tr -d '"' | tr -d "'" | cut -d' ' -f3)

poetry:
	pip install poetry==1.4

dependencies:
	poetry config virtualenvs.in-project true
	poetry add $(requirements)
	poetry add --group dev $(dev_requirements)
	poetry install

clean:
	rm -rf tests/tmp*
	rm -rf .venv
	rm -rf .pytest_cache
	rm -rf .ruff_cache
	rm -rf src/$(app_name)/__pycache__
	rm -rf tests/__pycache__
	rm -rf .coverage
	rm -rf coverage.xml
	rm -rf dist

format:
	poetry run black .

lint:
	poetry run ruff .

test:
	poetry run pytest

build:
	poetry build

prepare-documentation:
	rm -rf docs/clickgreet
	poetry run sphinx-quickstart  docs/$(app_name) --sep -p $(app_name) -a "Cavelle Benjamin" -v "$(version)" -r "$(version)" -l "en" --ext-autodoc --ext-doctest --extensions "sphinx-click"

build-documentation:
	poetry run sphinx-build docs/$(app_name)/source docs/$(app_name)/build

local-pipeline: clean format lint dependencies test build build-documentation

chimera:
	poetry run chimera $(arguments)

gadd:
	git add -A

gcommit:
	git commit --file commit.txt

gadd-commit: gadd gcommit

gpush:
	git push -u origin main